<?php

return [
    'name' => '阅读马拉松',
    'beian' => '',
    'cdnurl' => '',
    'version' => '1.0.1',
    'timezone' => 'Asia/Shanghai',
    'forbiddenip' => '',
    'languages' => [
        'backend' => 'zh-cn',
        'frontend' => 'zh-cn',
    ],
    'fixedpage' => 'dashboard',
    'categorytype' => [
        'default' => 'Default',
        'page' => 'Page',
        'article' => 'Article',
        'test' => 'Test',
    ],
    'configgroup' => [
        'basic' => 'Basic',
        'email' => 'Email',
        'dictionary' => 'Dictionary',
        'user' => '组别设置',
        'example' => '海报配置',
    ],
    'mail_type' => '1',
    'mail_smtp_host' => 'smtp.qq.com',
    'mail_smtp_port' => '465',
    'mail_smtp_user' => '10000',
    'mail_smtp_pass' => 'password',
    'mail_verify_type' => '2',
    'mail_from' => '10000@qq.com',
    'haibaobg' => '/uploads/20210214/b7e0d966d2c504475b49193b88157187.jpg',
    'read_img' => '',
    'group_name' => [
        '成人组' => '成人组',
        '儿童组' => '儿童组',
    ],
    'card_index_xieyi' => '<div class="index-module_textWrap_3ygOc" style="margin-top:22px;font-family:arial;font-size:12px;white-space:normal;background-color:#FFFFFF;"><p style="margin-top:0px;margin-bottom:0px;padding:0px;font-size:16px;line-height:24px;color:#333333;text-align:justify;"><span class="bjh-p">4月20日，湖北省召开了“县域经济督促工作会议”，湖北省省长王晓东出席。</span></p></div><div class="index-module_textWrap_3ygOc" style="margin-top:22px;font-family:arial;font-size:12px;white-space:normal;background-color:#FFFFFF;"><p style="margin-top:0px;margin-bottom:0px;padding:0px;font-size:16px;line-height:24px;color:#333333;text-align:justify;"><span class="bjh-p">此次会议意在“鞭策滞后县（市、区）对标先进、查找不足”。</span></p></div><div class="index-module_textWrap_3ygOc" style="margin-top:22px;font-family:arial;font-size:12px;white-space:normal;background-color:#FFFFFF;"><p style="margin-top:0px;margin-bottom:0px;padding:0px;font-size:16px;line-height:24px;color:#333333;text-align:justify;"><span class="bjh-p">面对坐在对面的县（市、区）政府主要负责人，王晓东说，“为什么条件差不多的县，发展差别却那么大？”</span></p></div>',
];
