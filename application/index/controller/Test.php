<?php
namespace app\index\controller;
use EasyWeChat\Factory;
use think\Session;
use app\common\controller\Frontend;

class Test extends Frontend
{
	protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';
    protected $wechat = null;
	// 初始化
	public function _initialize()
	{
		//$config = get_addon_config('wechat'); //此处 看文档末尾
		$config = [
		    'app_id' => 'wx378f7e0b3d35285b',
		    'secret' => '8e3f8f3e27b8edf50393250720aaeb5a',
		    // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
		    'response_type' => 'array',
		];
		$config['oauth']['scopes'] = ['snsapi_userinfo'];
		$config['oauth']['callback'] = "https://ydmls.zqgx.gov.cn/index.php/index/test/home"; //回调地址
		$this->wechat = Factory::officialAccount($config); 
	}
	public function index()
    {
    	/*if (empty(Session::get("wechat_user"))) {
        	$response = $this->wechat->oauth->scopes(['snsapi_userinfo'])->redirect();
        	$response->send();
        }*/
        // $token = Session::get("wechat_user")['token']->access_token;
        // 用户收货地址
        $js = $this->wechat->jssdk->buildConfig(['checkJsApi','openAddress'],$debug = true, $beta = false, $json = true); //openAddress是地址列表

        $this->assign("js",$js); // 到页面
        return $this->view->fetch();
    }
    public function home()
    {
    	$user = $this->wechat->oauth->user();
    	$userInfo = User::get(['open'=>$user->id]); // 数据库根据openid查找数据

    	if (!$userInfo) {
    		// 进行注册
    		//此处代码略过
    	}
    	// 进行登录
    	Session::set("wechat_user",$user->toArray()); // 将openid等信息转化为数组
    	Session::set("user_info",$userInfo->toArray()); //将数据表查询的信息转化为数组
    	$this->success("登录成功","index"); 
    }
}