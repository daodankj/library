<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use app\common\model\ScoreLog;
use think\Db;
use think\Session;
use think\Validate;
use tool\Wechat;

/**
 * 读者中心
 */
class Reader extends Frontend
{
    protected $layout = 'reader';
    protected $noNeedLogin = ['word_info'];
    protected $noNeedRight = ['*'];

    public function _initialize()
    {
        parent::_initialize();

    }

    //等级计算
    private function levelInfo(){
        $name = '';
        if ($this->auth->score>=1800) {
            $name = '阅读王者';
        }elseif($this->auth->score>=1600){
            $name = '阅读大师';
        }elseif($this->auth->score>=1300){
            $name = '阅读钻石勇士';
        }elseif($this->auth->score>=1000){
            $name = '阅读白金勇士';
        }elseif($this->auth->score>=800){
            $name = '阅读黄金勇士';
        }elseif($this->auth->score>=600){
            $name = '阅读白银勇士';
        }elseif($this->auth->score>=400){
            $name = '阅读青铜勇士';
        }elseif($this->auth->score>=200){
            $name = '阅读黑铁勇士';
        }
        return $name;
    }
    /**
     * 读者中心
     */
    public function index()
    {
        //活动信息
        $activeInfo = $this->getActiveInfo();
        //获取报名人数
        $bmNum = db('active_baoming')->where(['active_id'=>$activeInfo['id']])->count();
        //判断自己是否报名
        $bmInfo = db('active_baoming')->where(['user_id'=>$this->auth->id,'active_id'=>$activeInfo['id']])->find();
        $isBm = empty($bmInfo)?0:1;
        //获取签到次数
        if ($activeInfo) {
            $qdNum = db('user_score_log')->where(['user_id'=>$this->auth->id,'active_id'=>$activeInfo['id'],'type'=>1])->count();
        }else $qdNum = 0;
        //获取称号
        $levelName = $this->levelInfo();
        if (!$levelName&&$qdNum>0) {
            $levelName = '阅读勇士';
        }
        if ($levelName) {
            $levelName = "（".$levelName."）";
        }
        //判断今天是否已经签到
        $todayTime = strtotime(date('Y-m-d'));
        $isQd = db('user_score_log')->where(['user_id'=>$this->auth->id,'active_id'=>$activeInfo['id'],'type'=>1,'createtime'=>['>',$todayTime]])->count();
        //获取积分排行
        $topNum = db('user')->where('total_score>'.$this->auth->total_score)->count();
        //获取阅读书籍,从session中获取读者选择的数据
        $book = session('readbook');
        /*if (!$book) {
            $book = db('book')->where(['status'=>1])->order('id desc')->find();//默认一本
            session('readbook',$book);
        }*/
        //获取阅读感言
        $word_list = db('book_words')->where(['user_id'=>$this->auth->id])->order('id desc')->limit(10)->select();


        $this->view->assign('bmNum', $bmNum);
        $this->view->assign('qdNum', $qdNum);
        $this->view->assign('isQd', $isQd);
        $this->view->assign('isBm', $isBm);
        $this->view->assign('bmInfo', $bmInfo);
        $this->view->assign('topNum', $topNum+1);
        $this->view->assign('activeInfo', $activeInfo);
        $this->view->assign('book', $book);
        $this->view->assign('levelName', $levelName);
        $this->view->assign('word_list', $word_list);
        $this->view->assign('title', '首页');
        return $this->view->fetch();
    }
    //优秀感言排行
    public function words_list(){
        $list = db('book_words')->where(['status'=>1])->order('dz_num desc')->limit(100)->select();

        $this->view->assign('list', $list);
        $this->view->assign('title', '优秀感言');
        return $this->view->fetch();
    }
    //报名规则
    public function baominggz($type=1){
        $activeInfo = $this->getActiveInfo();
        if ($type==1) {
            $tname = '活动规则';
            $info = $activeInfo['active_rule'];
        }elseif($type==2){
            $tname = '积分规则';
            $info = $activeInfo['score_rule'];
        }else{
            $tname = '活动奖品';
            $info = $activeInfo['prize_name'];
        }



        $this->view->assign('info', $info);
        $this->view->assign('title', $tname);
        return $this->view->fetch();
    }

    //签到页面
    public function qiandao_list(){
        //活动信息
        $activeInfo = $this->getActiveInfo();
        //判断今天是否已经签到
        $todayTime = strtotime(date('Y-m-d'));
        $isQd = db('user_score_log')->where(['user_id'=>$this->auth->id,'active_id'=>$activeInfo['id'],'type'=>1,'createtime'=>['>',$todayTime]])->count();
        $list = db('user_score_log')->where(['user_id'=>$this->auth->id,'active_id'=>$activeInfo['id'],'type'=>1])->order('id desc')->select();
        $total = count($list);

        $this->view->assign('isQd', $isQd);
        $this->view->assign('list', $list);
        $this->view->assign('total', $total);
        $this->view->assign('title', '签到');
        return $this->view->fetch();
    }
    //积分排行
    public function score_top(){
        //活动信息
        $activeInfo = $this->getActiveInfo();

        $list = db('user')->field('id,username,total_score')->order('total_score desc')->limit(500)->select();
        foreach ($list as $key => &$val) {
            //获取报名信息里的书名
            $uname = db('active_baoming')->where(['user_id'=>$val['id'],'active_id'=>$activeInfo['id']])->value('uname');
            if ($uname) {
                $val['username'] = $uname;
            }elseif ($val['username']) {
        		$val['username'] = $this->hideName($val['username']);
        	}
        }
        //累计积分排行
        $list2 = db('user')->field('id,username,all_total_score as total_score')->order('total_score desc')->limit(500)->select();
        foreach ($list2 as $key => &$val) {
            //获取报名信息里的书名
            $uname = db('active_baoming')->where(['user_id'=>$val['id'],'active_id'=>$activeInfo['id']])->value('uname');
            if ($uname) {
                $val['username'] = $uname;
            }elseif ($val['username']) {
                $val['username'] = $this->hideName($val['username']);
            }
        }
        $this->view->assign('list', $list);
        $this->view->assign('list2', $list2);
        $this->view->assign('title', '积分排行');
        return $this->view->fetch();
    }
    //名字星号处理
    private function hideName($str){
    	//判断是否包含中文字符
		if(preg_match("/[\x{4e00}-\x{9fa5}]+/u", $str)) {
		    //按照中文字符计算长度
		    $len = mb_strlen($str, 'UTF-8');
		    //echo '中文';
		    if($len >= 3){
		        //三个字符或三个字符以上掐头取尾，中间用*代替
		        $str = mb_substr($str, 0, 1, 'UTF-8') . '*' . mb_substr($str, -1, 1, 'UTF-8');
		    } elseif($len == 2) {
		        //两个字符
		        $str = mb_substr($str, 0, 1, 'UTF-8') . '*';
		    }
		} else {
		    //按照英文字串计算长度
		    $len = strlen($str);
		    //echo 'English';
		    if($len >= 3) {
		        //三个字符或三个字符以上掐头取尾，中间用*代替
		        $str = substr($str, 0, 1) . '*' . substr($str, -1);
		    } elseif($len == 2) {
		        //两个字符
		        $str = substr($str2, 0, 1) . '*';
		    }
		}
		return $str;
    }

    //签到接口
    public function qiandao(){
        //活动信息
        $activeInfo = $this->getActiveInfo();
        if (!$activeInfo) {
            $this->error('暂无可签到的活动');
        }
        //判断今天是否已经签到
        $todayTime = strtotime(date('Y-m-d'));
        $isQd = db('user_score_log')->where(['user_id'=>$this->auth->id,'active_id'=>$activeInfo['id'],'type'=>1,'createtime'=>['>',$todayTime]])->count();
        if ($isQd) {
            $this->error('您今天已经签过到');
        }
        //最后一次签到时间
        $lastDtime = db('user_score_log')->where(['user_id'=>$this->auth->id,'active_id'=>$activeInfo['id'],'type'=>1])->order('id desc')->value('createtime');
        Db::startTrans();
        $scoreLog = ScoreLog::create(['user_id' => $this->auth->id,'active_id'=>$activeInfo['id'],'type'=>1, 'score' => $activeInfo['daka_score'], 'before' => $this->auth->score, 'after' => $this->auth->score + $activeInfo['daka_score'], 'memo' => '签到获取积分']);
        if ($scoreLog->id) {
            $rs = $this->auth->getUser()->save(['total_score' => $this->auth->total_score + $activeInfo['daka_score'], 'score' => $this->auth->score + $activeInfo['daka_score'],'all_total_score' => $this->auth->all_total_score + $activeInfo['daka_score'],]);
            if ($rs) {
                //计算累计打卡天数

                Db::commit();
                $this->success('签到成功');
            }
        }
        Db::rollback();
        $this->error('签到失败');
    }

    //报名页面
    public function baoming(){
        if ($this->request->isPost()) {
            $data = $this->request->post();
            $rule = [
                'group_name' => 'require',
                'library_name'  => 'require',
                'address'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'group_name' => '参与组别不能为空',
                'library_name' => '阅读场所不能为空',
                'address'  => '收件地址不能为空'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            //活动信息
            $activeInfo = $this->getActiveInfo();
            if (!$activeInfo) {
                $this->error('暂无可报名的活动');
            }
            //判断报名时间是否已经过
            if ($activeInfo['bm_starttime']>time()) {
                $this->error('还未到报名时间');
            }elseif($activeInfo['bm_endtime']<time()){
                $this->error('已超过报名时间，无法报名');
            }
            //判断自己是否报名
            $isBm = db('active_baoming')->where(['user_id'=>$this->auth->id,'active_id'=>$activeInfo['id']])->count();
            if ($isBm) {
                $this->error('您已报名，无需再次报名');
            }
            unset($data['__token__']);
            unset($data['library_name_text']);
            $data['active_id'] = $activeInfo['id'];
            $data['user_id'] = $this->auth->id;
            if (!$data['uname']) {
            	$data['uname'] = $this->auth->username;
            }
            $data['createtime'] = time();
            $res = db('active_baoming')->insert($data);
            if ($res) {
                $this->success('报名成功','index');
            }else{
                $this->error('报名失败');
            }
        }
        $this->view->assign('group_name', config('site.group_name'));
        $this->view->assign('title', '活动报名');
        return $this->view->fetch();
    }
    //修改阅读场所
    public function baoming_edit($id){
        $info = db('active_baoming')->where(['id'=>$id])->find();
        if ($this->request->isPost()) {
            $data = $this->request->post();
            $rule = [
                'library_name'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'library_name' => '阅读场所不能为空'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            $updatedata['id'] = $id;
            $updatedata['library_name'] = $data['library_name'];
            $updatedata['uname'] = $data['uname'];
            if (!$updatedata['uname']) {
            	$updatedata['uname'] = $this->auth->username;
            }
            $res = db('active_baoming')->update($updatedata);
            if ($res) {
                $this->success('修改成功','index');
            }else{
                $this->error('修改失败');
            }
        }
        $this->view->assign('info', $info);
        $this->view->assign('title', '修改阅读场所');
        return $this->view->fetch();
    }

    //选择书籍
    public function choose_book(){
        if ($this->request->isPost()) {
            $data = $this->request->post();
            /*$rule = [
                'book_id' => 'require',
                //'__token__' => 'require|token'
            ];

            $msg = [
                'book_id' => '请选择需要阅读的书籍'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }*/
            if (!$data['book_id']&&!$data['book_name']) {
                $this->error('请选择或输入您要阅读的书籍');
                return false;
            }
            if ($data['book_id']) {
                $book = db('book')->where(['id'=>$data['book_id']])->find();
            }elseif($data['book_name']){
                $book = db('book')->where(['book_name'=>$data['book_name']])->find();
                if (!$book) {//书籍不存在则新增
                    $rs = db('book')->insert(['book_name'=>$data['book_name'],'createtime'=>time()]);
                    if (!$rs) {
                        $this->error('选择失败');
                    }
                    $book = db('book')->where(['book_name'=>$data['book_name']])->find();
                }
            }

            if ($book) {
                session('readbook',$book);
                $this->success('选择成功','index');
            }
            $this->error('选择失败');
        }
        $this->view->assign('title', '选择书籍');
        return $this->view->fetch();
    }

    //发布感言
    public function my_word(){
        if ($this->request->isPost()) {
            $data = $this->request->post();
            $rule = [
                'book_id' => 'require',
                'content'  => 'require|length:1,150',
                '__token__' => 'require|token'
            ];

            $msg = [
                'book_id' => '请先选择书籍',
                'content.require' => '请输入阅读感言',
                'content.length' => '不能超过150个字符',
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            //活动信息
            $activeInfo = $this->getActiveInfo();
            if (!$activeInfo) {
                $this->error('暂无阅读活动', null, ['token' => $this->request->token()]);
            }
            //判断是否有资格发布
            $real_user = db('user_real')->where(['idcard'=>$this->auth->idcard])->value('id');
            if (!$real_user){
                $this->error('抱歉，您无发布资格！', null, ['token' => $this->request->token()]);
            }
            //判断是否报名
            $bmInfo = db('active_baoming')->where(['user_id'=>$this->auth->id,'active_id'=>$activeInfo['id']])->find();
            if (!$bmInfo) {
                $this->error('请先报名', null, ['token' => $this->request->token()]);
            }
            unset($data['__token__']);
            $data['active_id'] = $activeInfo['id'];
            $data['user_id'] = $this->auth->id;
            $data['uname'] = $bmInfo['uname'];//$this->auth->username;
            $data['createtime'] = time();
            $data['library_name'] = $bmInfo['library_name'];
            $data['status'] = 1;
            $res = db('book_words')->insert($data);
            if ($res) {
                //$id = db('book_words')->getLastInsID();
                //不用审核，直接加积分
                //一天只能获得一次积分
                $daystart = strtotime(date('Y-m-d'));
                $has_score = db('user_score_log')->where(['user_id' => $this->auth->id,'type'=>4,'createtime'=>['>=',$daystart]])->value('id');
                if ($activeInfo['words_score']&&!$has_score) {
                    $user_score = db('user')->where(['id'=>$this->auth->id])->field('total_score,score,all_total_score,successword_day,wordtime')->find();
                    Db::startTrans();
                    $scoreLog = ScoreLog::create(['user_id' => $this->auth->id,'active_id'=>$activeInfo['id'],'type'=>4, 'score' => $activeInfo['words_score'], 'before' => $user_score['score'], 'after' => $user_score['score'] + $activeInfo['words_score'], 'memo' => '发表感言获取积分']);
                    if ($scoreLog->id) {
                        //计算连续天数
                        if ($user_score['wordtime'] < \fast\Date::unixtime('day')) {
                            $successword_day = $user_score['wordtime'] < \fast\Date::unixtime('day', -1) ? 1 : $user_score['successword_day'] + 1;
                        }else{
                            $successword_day = $user_score['successword_day'];
                        }
                        $rs = db('user')->where(['id'=>$this->auth->id])->update(['total_score' => $user_score['total_score'] + $activeInfo['words_score'], 'score' => $user_score['score'] + $activeInfo['words_score'],'all_total_score' => $user_score['all_total_score'] + $activeInfo['words_score'],'successword_day'=>$successword_day,'wordtime'=>time()]);
                        if ($rs) {
                            Db::commit();
                        }else{
                            Db::rollback();
                        }
                    }
                }
                $this->success('发布成功','/index/reader');
            }else{
                $this->error('发布失败', null, ['token' => $this->request->token()]);
            }

        }

        $book = session('readbook');
        $this->view->assign('book', $book);
        $this->view->assign('title', '阅读感言发布');
        return $this->view->fetch();
    }

    //感言详情
    public function word_info($id){
        $info = db('book_words')->where(['id'=>$id])->find();
        if ($this->request->isPost()) {
            $data = $this->request->post();
            $rule = [
                'openId' => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'openId' => '请从微信端访问次页面'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            //判断是否已点赞
            $hasZ = db('like_log')->where(['word_id'=>$id,'openid'=>$data['openId']])->find();
            if ($hasZ) {
                $this->error('您已点过赞，不能重复点赞');
            }

            Db::startTrans();
            $createtime = time();
            //点赞日志
            $rs = db('like_log')->insert(['word_id'=>$id,'openid'=>$data['openId'],'createtime'=>$createtime]);
            if (!$rs) {
                Db::rollback();
                $this->error('点赞失败');
            }
            //更新点赞数
            $rs = db('book_words')->where(['id'=>$id])->setInc('dz_num',1);
            if (!$rs) {
                Db::rollback();
                $this->error('点赞失败');
            }
            //判断当日点赞积分是否超过30，每天最多30积分
            /*$todayBeginTime = strtotime(date('Y-m-d'));
            $todayEndTime = strtotime('+1 day',$todayBeginTime);
            $where = [
                'user_id' => $info['user_id'],
                'type'=>2,
                'createtime'=>['between',[$todayBeginTime,$todayEndTime]]
            ];
            $dzNum = db('user_score_log')->where($where)->count();
            if ($dzNum>=30) {
                Db::commit();
                $this->success('点赞成功');
            }*/
            //加积分
            $score = db('active')->where(['id'=>$info['active_id']])->value('zan_score');
            if(!$score){
                Db::commit();
                $this->success('点赞成功');
            }
            //更新积分
            $user_score = db('user')->where(['id'=>$info['user_id']])->field('total_score,score,all_total_score')->find();
            $rs = db('user')->where(['id'=>$info['user_id'],'score'=>$user_score['score']])->update(['total_score'=>$user_score['total_score'] + $score,'score'=>$user_score['score'] + $score,'all_total_score'=>$user_score['all_total_score'] + $score]);
            if (!$rs) {
                Db::rollback();
                $this->error('点赞失败');
            }
            //积分日志
            $scoreLog = ScoreLog::create(['user_id' => $info['user_id'],'active_id'=>$info['active_id'],'word_id'=>$id,'type'=>2, 'score' => $score, 'before' => $user_score['score'], 'after' => $user_score['score'] + $score, 'memo' => '点赞获取积分']);
            if (!$scoreLog->id) {
                Db::rollback();
                $this->error('点赞失败');
            }

            Db::commit();
            $this->success('点赞成功');

        }
        $openId = 'testcode2';
        //获取微信openid
        $redirect_uri = url('index/reader/word_info',['id'=>$id],'',true);
        $wchate = new Wechat(['callback'=>$redirect_uri]);
        $userinfo = $wchate->getUserInfo();
        if (isset($userinfo['openid'])) {
            $openId = $userinfo['openid'];
        }
        //print_r($userinfo);echo $openId;exit;
        //判断是否已点赞
        $isDz = db('like_log')->where(['word_id'=>$id,'openid'=>$openId])->value('id');
        //读者名称从报名信息获取
        $bmInfo = db('active_baoming')->where(['user_id'=>$info['user_id'],'active_id'=>$info['active_id']])->find();
        $info['uname'] = $bmInfo['uname'];

        $this->view->assign('isDz',$isDz);
        $this->view->assign('openId',$openId);
        $this->view->assign('info',$info);
        $this->view->assign('title', '感言详情');
        return $this->view->fetch();
    }
    // 生成二维码
    private function save_qrcode($text,$isSaveName)
    {
        $params = [
            'text'           => $text,
            'size'           => 350,
            'padding'        => 10,
            'errorlevel'     => 'medium',
            'foreground'     => '#000000',
            'background'     => '#ffffff',
            'logo'           => '',
            'logosize'       => '',
            'label'          => '',
            'labelfontsize'  => '',
            'labelalignment' => '',
        ];

        $qrCode = \addons\qrcode\library\Service::qrcode($params);
        // 写入到文件
        if ($isSaveName) {
            $filePath = ROOT_PATH . 'public/uploads/qrcode/' . $isSaveName . '.png';
            $qrCode->writeFile($filePath);
        }
        return true;
    }
    //生成海报
    public function create_img($id){
    	$info = db('book_words')->where('id='.$id)->find();
    	$this->view->assign('status', $info['status']);
    	if ($info['status']!=1) {//未审核不显示海报
    		$this->view->assign('hb_url', '');
        	$this->view->assign('title', '海报下载');
        	return $this->view->fetch();
    	}
        $isSaveName = 'word_'.$id;
        if (!file_exists(ROOT_PATH . 'public/uploads/qrcode/'. $isSaveName.'.png')) {
            $url = url('word_info',['id'=>$id],'',true);
            $this->save_qrcode($url,$isSaveName);
            //$imgurl = addon_url('qrcode/index/build',[],false).'?text='.$url.'&isSaveName='.$isSaveName;
            //echo '<img style="display:none;" src="'.$imgurl.'" />';
        }
        if (file_exists(ROOT_PATH . 'public/uploads/haibao/'. $isSaveName.'.png')) {
            $hb_url = '/uploads/haibao/'. $isSaveName.'.png';
        }else if (file_exists(ROOT_PATH . 'public/uploads/qrcode/'. $isSaveName.'.png')) {
            //查询签到多少次
            $qdNum = db('user_score_log')->where(['user_id'=>$this->auth->id,'active_id'=>$info['active_id'],'type'=>1])->count();
            $qrimg = ROOT_PATH . 'public/uploads/qrcode/'. $isSaveName.'.png';
            $simg = ROOT_PATH . 'public'.$info['image'];
            $image = \think\Image::open(ROOT_PATH.'public'.config('site.haibaobg'));
            $savePath = ROOT_PATH . 'public/uploads/haibao/'. $isSaveName.'.png';
            //$image->water($qrimg,\think\Image::WATER_CENTER)->save($savePath);
            $image->text("《".$info['book_name']."》",ROOT_PATH.'public/simkai.ttf',35,'#0080FF',[100,570]);
            $image->text('已打卡'.$qdNum.'天',ROOT_PATH.'public/simkai.ttf',35,'#ff0000',[750,570]);
            $lenth = mb_strlen($info['content'],'utf8');
            if ($lenth<=15) {
                $words = mb_substr($info['content'], 0,15);
                $image->text($words,ROOT_PATH.'public/simkai.ttf',35,'#000000',[180,660]);
            }elseif($lenth<=30){
                $words1 = mb_substr($info['content'], 0,15);
                $image->text($words1,ROOT_PATH.'public/simkai.ttf',35,'#000000',[180,660]);
                $words2 = mb_substr($info['content'], 15,15);
                $image->text($words2,ROOT_PATH.'public/simkai.ttf',35,'#000000',[140,710]);
            }else{
                $words1 = mb_substr($info['content'], 0,15);
                $image->text($words1,ROOT_PATH.'public/simkai.ttf',35,'#000000',[180,660]);
                $words2 = mb_substr($info['content'], 15,15);
                $image->text($words2,ROOT_PATH.'public/simkai.ttf',35,'#000000',[140,710]);
                $words3 = mb_substr($info['content'], 30,15);
                $image->text($words3."....",ROOT_PATH.'public/simkai.ttf',35,'#000000',[140,760]);
            }
            if ($info['image']) {
                $image->water($simg,[200,850]);
                //$image->text($info['content'],ROOT_PATH.'public/simkai.ttf',18,'#000000',[100,600])->water($simg,[100,1000])->water($qrimg,[650,1400])->save($savePath);
            }elseif(config('site.read_img')){
            	$simg = ROOT_PATH . 'public'.config('site.read_img');
            	$image->water($simg,[200,850]);
            }

            $image->water($qrimg,[650,1400])->save($savePath);

            //$image->text('API开发设计的高性能框架',ROOT_PATH.'public/simkai.ttf',20,'#ffffff',1)->water($qrimg,\think\Image::WATER_CENTER)->save($savePath);
            //echo '生成海报成功';
            $hb_url = '/uploads/haibao/'. $isSaveName.'.png';
        }else{
            //echo '海报生成失败';
            $hb_url = '';
        }

        $this->view->assign('hb_url', $hb_url);
        $this->view->assign('title', '海报下载');
        return $this->view->fetch();
    }

    //电子书屋
    public function card_index(){
        $info = db('card_baoming')->where(['idcard'=>$this->auth->idcard])->order('id desc')->find();
        if ($info) {
            $info['idcard'] = substr_replace($info['idcard'],'********',4,8);
        }
        $library = db('library')->where(['id'=>['in','5,6']])->field('id,library_name,image')->select();

        $this->view->assign('info', $info);
        $this->view->assign('library', $library);
        $this->view->assign('title', '职工电子书屋');
        return $this->view->fetch();
    }
    public function card_intro(){
        $this->view->assign('title', '使用说明');
        return $this->view->fetch();
    }
    //图书馆详情
    public function library_info($id){
        $info = db('library')->where(['id'=>$id])->find();

        $this->view->assign('info', $info);
        $this->view->assign('title', '图书馆介绍');
        return $this->view->fetch();
    }

    //办理图书卡报名
    public function card_baoming(){
        $info = db('card_baoming')->where(['idcard'=>$this->auth->idcard,'status'=>['<',2]])->order('id desc')->find();
        if ($this->request->isPost()) {
            $data = $this->request->post();
            $rule = [
                'name'  => 'require',
                'idcard'  => 'require',
                'phone'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'name' => '姓名必须',
                'idcard' => '身份证必须',
                'phone' => '电话必须'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->auth->id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('card_baoming')->insert($data);
            if ($res) {
                $this->success('报名成功','card_index');
            }else{
                $this->error('报名失败');
            }
        }
        if ($info){
            //生成条形码
            $cardno = '10000'.$info['id'];
            if ($cardno) {
                $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
                $barcode = $generator->getBarcode($cardno,$generator::TYPE_CODE_128,2,80);
                $barcode = base64_encode($barcode);
                $barcode = "data:image/png;base64,".$barcode;
                $info['cardimg'] = $barcode;
            }
        }
        $this->view->assign('card_index_xieyi', config('site.card_index_xieyi'));
        $this->view->assign('info', $info);
        $this->view->assign('title', '读者证申请');
        return $this->view->fetch();
    }

    //获取最新活动信息
    private function getActiveInfo(){
        $time = time();
        $where = [
            'status'    =>1,
            'starttime' =>['<',$time],
            'endtime'   =>['>',$time]
        ];
        $info = db('active')->where($where)->order('id desc')->find();
        return $info;
    }




    //商品列表
    public function goods_list(){
        $list = db('good')->where(['status'=>1])->field('id,good_name,image,price,score')->select();

        $this->view->assign('list', $list);
        $this->view->assign('title', '商品兑换');
        return $this->view->fetch();
    }

    //商品详情
    public function goods_info($id){
        $info = db('good')->where(['id'=>$id])->find();

        $this->view->assign('info', $info);
        $this->view->assign('title', '商品详情');
        return $this->view->fetch();
    }

    //商品兑换
    public function good_dh($id){
        if(!$id){
            $this->error('兑换失败');
        }
        $goodInfo = db('good')->where(['id'=>$id,'status'=>1])->find();
        if (!$goodInfo) {
            $this->error('商品不存在或已下架');
        }
        if ($this->auth->score<$goodInfo['score']) {
            $this->error('您的可用积分不足');
        }
        Db::startTrans();
        //生成订单
        $data = [
            'user_id' => $this->auth->id,
            'uname'   => $this->auth->username,
            'shop_id' => $goodInfo['shop_id'],
            'good_id' => $id,
            'good_name'=> $goodInfo['good_name'],
            'score'   => $goodInfo['score'],
            'createtime'=>time()
        ];
        $rs = db('order')->insert($data);
        if (!$rs) {
            Db::rollback();
            $this->error('兑换失败');
        }
        //扣减积分
        $rs = db('user')->where(['id'=>$this->auth->id,'score'=>$this->auth->score])->setDec('score',$goodInfo['score']);
        if (!$rs) {
            Db::rollback();
            $this->error('兑换失败');
        }
        //积分日志
        $scoreLog = ScoreLog::create(['user_id' => $this->auth->id,'type'=>3, 'score' => 0-$goodInfo['score'], 'before' => $this->auth->score, 'after' => $this->auth->score - $goodInfo['score'], 'memo' => '商品兑换']);
        if (!$scoreLog->id) {
            Db::rollback();
            $this->error('兑换失败');
        }

        Db::commit();
        $this->success('兑换成功');
    }

    //兑换记录
    public function order_list(){
        $list = db('order')->where(['user_id'=>$this->auth->id])->order('createtime desc')->limit(30)->select();
        foreach ($list as $key => &$val) {
            $info = db('good')->where(['id'=>$val['good_id']])->field('image,price,address')->find();
            $val['image'] = $info['image'];
            $val['price'] = $info['price'];
            $val['address'] = $info['address'];
            $val['url'] = url('index/reader/order_check',['id'=>$val['id']],'',true);
        }
        $this->view->assign('list', $list);
        $this->view->assign('title', '兑换记录');
        return $this->view->fetch();
    }

    //订单核销
    public function order_check($id){
        $info = db('order')->where(['id'=>$id])->find();
        if ($this->request->isPost()) {
            $data = $this->request->post();
            $rule = [
                'code' => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'code' => '核销码必须'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            $shop_code = db('shop')->where(['id'=>$info['shop_id']])->value('shop_code');
            if ($shop_code!=$data['code']) {
                $this->error('核销码错误', null, ['token' => $this->request->token()]);
            }
            $rs = db('order')->where(['id'=>$id])->update(['status'=>1]);
            if ($rs) {
                session('check_code',$data['code']);
                $this->success('核销成功');
            }else{
                $this->error('核销码失败', null, ['token' => $this->request->token()]);
            }
        }

        $dinfo = db('good')->where(['id'=>$info['good_id']])->field('image,price')->find();
        $info['image'] = $dinfo['image'];
        $info['price'] = $dinfo['price'];
        //会员信息
        $uinfo = db('user')->where(['id'=>$info['user_id']])->field('username,idcard,mobile')->find();

        $this->view->assign('check_code', session('check_code'));
        $this->view->assign('uinfo', $uinfo);
        $this->view->assign('info', $info);
        $this->view->assign('title', '订单核销');
        return $this->view->fetch();
    }

}
