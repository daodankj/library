<?php
namespace app\index\controller;
use app\common\controller\Frontend;
use app\common\library\Sms;

class Crontab extends Frontend
{
	protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

	
	public function card_notice()
    {
        $starttime = \fast\Date::unixtime('day', -3);//3天前
        $sdate = date('Y-m-d H:i:s');
        $num = db('card_baoming')->where(['status'=>1,'add_time'=>['<',$sdate]])->count();//3天前未审核的
    	if ($num>0) {
            Sms::notice('13602993551,18578555590','您还有3天前的读者证申请未处理，请及时处理！','');
        }
        return 'OK';
    }
    
}