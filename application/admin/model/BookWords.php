<?php

namespace app\admin\model;

use think\Model;


class BookWords extends Model
{

    

    

    // 表名
    protected $name = 'book_words';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'active_name'
    ];

    protected function getActiveNameAttr($value,$data){
        $name = db('active')->where(['id'=>$data['active_id']])->value('active_name');

        return $name?$name:'';
    }
    

    







}
