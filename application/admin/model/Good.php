<?php

namespace app\admin\model;

use think\Model;


class Good extends Model
{

    

    

    // 表名
    protected $name = 'good';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'shop_name'
    ];
    

    protected function getShopNameAttr($value,$data){
        $shop_name = db('shop')->where('id='.$data['shop_id'])->value('shop_name');

        return $shop_name?$shop_name:'';
    }







}
