<?php

return [
    'User_id'      => '会员ID',
    'Active_id'    => '活动id',
    'Uname'        => '用户名称',
    'Group_name'   => '参与组别',
    'Library_name' => '参与场所',
    'Address'      => '收件地址',
    'Createtime'   => '创建时间'
];
