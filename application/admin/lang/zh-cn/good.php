<?php

return [
    'Shop_id'   => '所属商家',
    'Good_name' => '商品名称',
    'Image'     => '商品图片',
    'Intro'     => '商品详情',
    'Status'    => '上下架',
    'Score'     => '兑换积分',
    'Address'   => '兑换地址'
];
