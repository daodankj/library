<?php

return [
    'User_id'    => '读者',
    'Uname'      => '读者姓名',
    'Active_id'  => '所属活动',
    'Book_id'    => '阅读书籍',
    'Book_name'  => '书籍名称',
    'Content'    => '阅读感言',
    'Createtime' => '发布时间',
    'Dz_num'     => '点赞数'
];
