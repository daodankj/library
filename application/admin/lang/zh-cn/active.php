<?php

return [
    'Active_name' => '活动名称',
    'Active_rule' => '活动规则',
    'Score_rule'  => '积分规则',
    'Prize_name'  => '奖励奖品',
    'Status'      => '状态',
    'Starttime'   => '开始时间',
    'Endtime'     => '结束时间',
    'Daka_score'  => '打卡积分数',
    'Zan_score'   => '点赞积分数'
];
