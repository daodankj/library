<?php

return [
    'User_id'  => '用户',
    'Name'     => '姓名',
    'Idcard'   => '身份证号码',
    'Phone'    => '手机号码',
    'Add_time' => '报名时间'
];
