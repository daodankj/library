<?php

return [
    'User_id'    => '所属用户',
    'Uname'      => '用户姓名',
    'Shop_id'    => '商家',
    'Good_id'    => '兑换商品',
    'Good_name'  => '商品名称',
    'Score'      => '使用积分',
    'Createtime' => '兑换时间',
    'Status'     => '状态'
];
