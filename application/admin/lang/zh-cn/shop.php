<?php

return [
    'Shop_name'  => '商家名称',
    'Image'      => '商家Log',
    'Link_name'  => '联系人',
    'Link_phone' => '联系电话',
    'Intro'      => '介绍',
    'Shop_code'  => '核销码',
    'Createtime' => '创建时间'
];
