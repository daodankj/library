<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use app\common\model\ScoreLog;
use think\Db;

/**
 * 阅读感言
 *
 * @icon fa fa-circle-o
 */
class BookWords extends Backend
{
    
    /**
     * BookWords模型对象
     * @var \app\admin\model\BookWords
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\BookWords;

    }

    public function import()
    {
        parent::import();
    }

    //审批
    public function check($ids){
        if ($this->request->isPost()) {
            $id = $this->request->post('id');
            $status = $this->request->post('status');
            $result = $this->request->post('result');
            if (!$result) {
                //$result = $this->request->post('result2');
                $result = $status==1?'情况已核实，给予通过':'情况不属实，不通过';
            }
            if (!$result) {
                $this->error('请输入审核意见');
            }
            $info = $this->model->get($id);
            if (!$info) {
                $this->error('该记录不存在，审核失败'.$id);
            }
            if ($info->status!=0) {
                $this->success('改记录已被审核，不用重新审核');
            }
            $info->status = $status;
            $info->remark = $result;
            if ($info->save()) {
                //增加积分
                $activeInfo = db('active')->where(['id'=>$info->active_id])->find();
                if ($activeInfo['words_score']>0&&$info->status==1) {
                    $user_score = db('user')->where(['id'=>$info->user_id])->field('total_score,score,all_total_score')->find();
                    Db::startTrans();
                    $scoreLog = ScoreLog::create(['user_id' => $info->user_id,'active_id'=>$activeInfo['id'],'type'=>4, 'score' => $activeInfo['words_score'], 'before' => $user_score['score'], 'after' => $user_score['score'] + $activeInfo['words_score'], 'memo' => '发表感言获取积分']);
                    if ($scoreLog->id) {
                        $rs = db('user')->where(['id'=>$info->user_id])->update(['total_score' => $user_score['total_score'] + $activeInfo['words_score'], 'score' => $user_score['score'] + $activeInfo['words_score'],'all_total_score' => $user_score['all_total_score'] + $activeInfo['words_score']]);
                        if ($rs) {
                            Db::commit();
                        }else{
                            Db::rollback();
                        }
                    }
                    
                }  
                $this->success('审核成功');
            }else{
                $this->error('审核失败');
            }
            
        }
        $this->view->assign('id',$ids);
        return $this->view->fetch();
    }
    

}
