<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 书籍管理
 *
 * @icon fa fa-book
 */
class Book extends Backend
{
    
    protected $noNeedLogin = ['index'];
    /**
     * Book模型对象
     * @var \app\admin\model\Book
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Book;

    }

    public function import()
    {
        parent::import();
    }

    
    

}
