<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use app\common\library\Sms;
use think\Db;

/**
 * 办理图书卡报名
 *
 * @icon fa fa-circle-o
 */
class CardBaoming extends Backend
{

    /**
     * CardBaoming模型对象
     * @var \app\admin\model\CardBaoming
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\CardBaoming;

    }

    public function import()
    {
        parent::import();
    }

    //审批
    public function check($ids){
        if ($this->request->isPost()) {
            $id = $this->request->post('id');
            $status = $this->request->post('status');
            $result = $this->request->post('result');
            if (!$result) {
                //$result = $this->request->post('result2');
                $result = $status==1?'情况已核实，给予通过':'情况不属实，不通过';
            }
            if (!$result) {
                $this->error('请输入审核意见');
            }
            $info = $this->model->get($id);
            if (!$info) {
                $this->error('该记录不存在，审核失败'.$id);
            }
            if ($info->status!=0) {
                $this->success('改记录已被审核，不用重新审核');
            }
            $info->status = $status;
            $info->remark = $result;
            if ($info->save()) {
                if ($status==1) {
                    $idcard = $info['idcard'] = substr_replace($info['idcard'],'********',4,8);
                    $msg = "尊敬的{$info['name']}，您的读者证已申请通过，账户为身份证号{$idcard}，密码为身份证后6位";
                 }else{
                    $msg = "尊敬的{$info['name']}，您的读者证申请已被拒绝";
                 }
                $result = Sms::notice($info['phone'],$msg,'');
                $this->success('审核成功');
            }else{
                $this->error('审核失败');
            }

        }
        $this->view->assign('id',$ids);
        return $this->view->fetch();
    }
    //批量审核
    public function mutcheck(){
        if ($this->request->isPost()) {
            $ids = $this->request->post('ids');
            $status = $this->request->post('status');
            if (!$ids){
                $this->error('请选择记录');
            }
            $ids = explode(',',$ids);
            //Db::startTrans();
            foreach ($ids as $key=>$val){
                $info = $this->model->get($val);
                if ($info && $info->status==0){
                    $info->status = $status;
                    $info->save();
                    if ($status==1){
                        $idcard = $info['idcard'] = substr_replace($info['idcard'],'********',4,8);
                        $msg = "尊敬的{$info['name']}，您的读者证已申请通过，账户为身份证号{$idcard}，密码为身份证后6位";
                        Sms::notice($info['phone'],$msg,'');
                    }else{
                        $msg = "尊敬的{$info['name']}，您的读者证申请已被拒绝";
                        Sms::notice($info['phone'],$msg,'');
                    }

                }
            }
            $this->success('审核成功');
        }
    }
}
