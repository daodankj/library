<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 图书馆管理
 *
 * @icon fa fa-circle-o
 */
class Library extends Backend
{

    protected $noNeedLogin = ['index'];
    /**
     * Library模型对象
     * @var \app\admin\model\Library
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Library;

    }

    public function import()
    {
        parent::import();
    }

    
    

}
