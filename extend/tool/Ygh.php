<?php

namespace tool;

use fast\Http;

/**
 * 粤工会接口
 */
class Ygh
{
    //const SEND_URL = "http://sms.189ek.com/yktsms/send";
    private $url;
    private $appid;
    private $appsecret;
    /**
     * 构造函数
     *
     * @param string $appid  sdkappid
     * @param string $appkey sdkappid对应的appkey
     */
    public function __construct($appid='3cdc840e5db34b438a944441aef9c078', $appsecret='1acd3b3c963b436cb83dabb7fe213589')
    {
        $this->url = "https://wx.gdftu.org.cn/api/phfw/";
        $this->appid =  $appid;
        $this->appsecret = $appsecret;
    }


    /**
     * 获取token
     * @param string mobile
     * @return array
     */
    public function getToken()
    {
        $data = array(
            "appid"      => $this->appid,
            "appsecret"  => $this->appsecret
        );
        $url = $this->url."getToken";
        $response = Http::post($url,$data);
        $result = json_decode($response,true);
        return $result ? $result : [];
    }

    //获取用户信息
    public function getInfo($token='',$unique='',$code=''){
        $data = array(
            "token"   => $token,
            "unique"  => $unique,
            "code"    => $code
        );
        $url = $this->url."getInfo";
        $response = Http::post($url,$data);
        $result = json_decode($response,true);
        return $result ? $result : [];
    }
}
