define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'order/index' + location.search,
                    add_url: 'order/add',
                    edit_url: 'order/edit',
                    del_url: 'order/del',
                    multi_url: 'order/multi',
                    import_url: 'order/import',
                    table: 'order',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        /*{field: 'user_id', title: __('User_id')},*/
                        {field: 'uname', title: __('Uname'), operate: 'LIKE'},
                        {field: 'shop_id', title: __('shop_id'),visible:false,addClass: "selectpage", extend: "data-source='shop/index' data-field='shop_name'"},
                        {field: 'shop_name', title: __('shop_id'),operate:false},
                        /*{field: 'good_id', title: __('Good_id')},*/
                        {field: 'good_name', title: __('Good_name'), operate: 'LIKE'},
                        {field: 'score', title: __('Score')},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'status', title: __('Status'),searchList: {"0":'待核销',"1":'已核销'},formatter: Controller.api.formatter.status},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                intro: function (value, row, index) {
                    var intro = value.substring(0,35);
                    return '<span data-toggle="tooltip" title="'+value+'">'+intro+'</span>';
                },
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="green">待核销</font>';
                    }else if(value==1){
                        return '<font color="#FF9800">已核销</font>';
                    }else{
                        return '<font color="red">已拒绝</font>';
                    }
                },
            }
        }
    };
    return Controller;
});