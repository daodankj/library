define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'book_words/index' + location.search,
                    add_url: 'book_words/add',
                    edit_url: 'book_words/edit',
                    del_url: 'book_words/del',
                    multi_url: 'book_words/multi',
                    import_url: 'book_words/import',
                    table: 'book_words',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                //fixedColumns:true,
                //fixedNumber:1,
                //fixedRightNumber:1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        /*{field: 'user_id', title: __('User_id'),visible:false},*/
                        {field: 'uname', title: __('Uname'), operate: 'LIKE'},
                        {field: 'active_id', title: __('Active_id'),visible:false,addClass: "selectpage", extend: "data-source='active/index' data-field='active_name'"},
                        {field: 'active_name', title: __('Active_id'),operate:false},
                        /*{field: 'book_id', title: __('Book_id'),visible:false},*/
                        {field: 'book_name', title: __('Book_name'), operate: 'LIKE'},
                        {field: 'content', title: __('Content'), operate:false,formatter: Controller.api.formatter.intro},
                        {field: 'image', title: __('图片'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'dz_num', title: __('Dz_num'),sortable:true},
                        {field: 'status', title: __('审核状态'),searchList: {"0":'待审核',"1":'审核通过',"2":'审核拒绝'},formatter: Controller.api.formatter.status,sortable: true},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'check',
                                text: __('审核'),
                                icon: 'fa fa-caret-right',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'book_words/check',
                                extend:'data-area=["400px","450px"]',
                                hidden:function(value,row){
                                    if(value.status == 0){
                                        return false;
                                    }else return true;
                                },
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            }
                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        check: function (){
            Controller.api.bindevent();
            $(".check").click(function(){
                var id = $("#c-id").val();
                var result = $("#c-result").val();
                //var result2 = $("#c-result2").val();
                var status = $(this).attr('data-status');
                /*if (!result) {
                    layer.alert('请输入意见');
                    return false;
                }*/
                var msg = status==1?'确认通过吗？':'确认不通过吗？'
                Layer.confirm(msg, function (index) {
                    layer.close(index);
                    var loadindex = Layer.load();
                    $(".check").addClass("disabled");
                    $.post({
                        url: '',
                        dataType: 'json',
                        data:{id:id,status:status},
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    parent.Toastr.success(msg ? msg : '审核成功');
                                    //Toastr.success(msg ? msg : '审核成功');
                                    //layer.close(loadindex);
                                    //window.location.reload();
                                    //Layer.closeAll();
                                    parent.$(".btn-refresh").trigger("click");
                                    var index = parent.Layer.getFrameIndex(window.name);
                                    parent.Layer.close(index);
                                } else {
                                    Toastr.error(msg ? msg : '审核失败');
                                    layer.close(loadindex);
                                }
                                $(".check").removeClass("disabled");
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                                $(".check").removeClass("disabled");
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                            $(".check").removeClass("disabled");
                        }
                    });
                });       
            }) 
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                intro: function (value, row, index) {
                    var intro = value.substring(0,35);
                    return '<span data-toggle="tooltip" title="'+value+'">'+intro+'</span>';
                },
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="#FF9800">待审核</font>';
                    }else if(value==1){
                        return '<font color="green">审核通过</font>';
                    }else{
                        return '<font color="red">审核拒绝</font>';
                    }
                },
            }
        }
    };
    return Controller;
});