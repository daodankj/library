define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'good/index' + location.search,
                    add_url: 'good/add',
                    edit_url: 'good/edit',
                    del_url: 'good/del',
                    multi_url: 'good/multi',
                    import_url: 'good/import',
                    table: 'good',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'shop_name', title: __('shop_id'), operate: false},
                        {field: 'shop_id', title: __('Shop_id'),visible:false,addClass: "selectpage", extend: "data-source='shop/index' data-field='shop_name'"},
                        {field: 'good_name', title: __('Good_name'), operate: 'LIKE'},
                        {field: 'image', title: __('Image'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'score', title: __('Score')},
                        {field: 'address', title: __('Address'), operate: 'LIKE'},
                        {field: 'status', title: __('Status'),searchList: {"0":'下架',"1":'上架'},formatter:Table.api.formatter.toggle},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});