define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        qiandao: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'ranking/qiandao' + location.search,
                    table: 'ranking',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'sum',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),visible:false,operate:false},
                        {field: 'active_id', title: __('活动'), visible: false,addClass: "selectpage", extend: "data-source='active/index' data-field='active_name'"},
                        {field: 'user_id', title: __('参与者'),visible: false, addClass: "selectpage", extend: "data-source='user/user/index' data-field='username'"},
                        {field: 'user_name', title: __('家长'), operate: false},
                        {field: 'child_name', title: __('学生'), operate: false},
                        {field: 'school_name', title: __('阅读场所'), operate: false},
                        {field: 'mobile', title: __('手机'), operate: false},
                        {field: 'idcard', title: __('身份证'), operate: false},
                        {field: 'sum', title: __('签到数'), operate: false,sortable:true},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        dianzhan: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'ranking/dianzhan' + location.search,
                    table: 'ranking',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'sum',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),visible:false,operate:false},
                        {field: 'active_id', title: __('活动'), visible: false,addClass: "selectpage", extend: "data-source='active/index' data-field='active_name'"},
                        {field: 'user_id', title: __('参与者'),visible: false, addClass: "selectpage", extend: "data-source='user/user/index' data-field='username'"},
                        {field: 'user_name', title: __('家长'), operate: false},
                        {field: 'child_name', title: __('学生'), operate: false},
                        {field: 'school_name', title: __('阅读场所'), operate: false},
                        {field: 'mobile', title: __('手机'), operate: false},
                        {field: 'idcard', title: __('身份证'), operate: false},
                        {field: 'sum', title: __('点赞数'), operate: false,sortable:true},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        words: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'ranking/words' + location.search,
                    table: 'user',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'successword_day',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'username', title: __('家长'), operate: 'LIKE'},
                        {field: 'child_name', title: __('学生'), operate: false},
                        {field: 'school_name', title: __('阅读场所'), operate: false},
                        {field: 'idcard', title: __('身份证'), operate: 'LIKE'},
                        {field: 'mobile', title: __('电话'), operate: 'LIKE'},
                        {field: 'successword_day', title: __('连续发表天数'), operate: 'BETWEEN'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        total_score: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'ranking/total_score' + location.search,
                    table: 'user',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'total_score',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'username', title: __('家长'), operate: 'LIKE'},
                        {field: 'child_name', title: __('学生'), operate: false},
                        {field: 'school_name', title: __('阅读场所'), operate: false},
                        {field: 'idcard', title: __('身份证'), operate: 'LIKE'},
                        {field: 'mobile', title: __('电话'), operate: 'LIKE'},
                        {field: 'total_score', title: __('赛季总积分'), operate: 'BETWEEN'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        all_total_score: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'ranking/all_total_score' + location.search,
                    table: 'user',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'all_total_score',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'username', title: __('家长'), operate: 'LIKE'},
                        {field: 'child_name', title: __('学生'), operate: false},
                        {field: 'school_name', title: __('阅读场所'), operate: false},
                        {field: 'idcard', title: __('身份证'), operate: 'LIKE'},
                        {field: 'mobile', title: __('电话'), operate: 'LIKE'},
                        {field: 'all_total_score', title: __('总积分'), operate: 'BETWEEN'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
    };
    return Controller;
});
