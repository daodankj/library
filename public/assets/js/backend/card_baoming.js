define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'card_baoming/index' + location.search,
                    add_url: 'card_baoming/add',
                    edit_url: 'card_baoming/edit',
                    del_url: 'card_baoming/del',
                    multi_url: 'card_baoming/multi',
                    import_url: 'card_baoming/import',
                    table: 'card_baoming',
                }
            });

            var table = $("#table");
            //在表格内容渲染完成后回调的事件
            table.on('post-body.bs.table', function (e, json) {
                $("tbody tr[data-index]", this).each(function () {
                    if ($("td:eq(7)", this).text() != '待审核') {
                        $("input[type=checkbox]", this).prop("disabled", true);
                    }
                });
            });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        /*{field: 'user_id', title: __('User_id')},*/
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'idcard', title: __('Idcard'), operate: 'LIKE'},
                        {field: 'sex', title: __('性别'), operate: false},
                        {field: 'phone', title: __('Phone'), operate: 'LIKE'},
                        {field: 'address', title: __('家庭地址'), operate: false},
                        {field: 'status', title: __('审核状态'),searchList: {"0":'待审核',"1":'审核通过',"2":'审核拒绝'},formatter: Controller.api.formatter.status,sortable: true},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'check',
                                text: __('审核'),
                                icon: 'fa fa-caret-right',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'card_baoming/check',
                                extend:'data-area=["400px","450px"]',
                                hidden:function(value,row){
                                    if(value.status == 0){
                                        return false;
                                    }else return true;
                                },
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            }
                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            var submitForm = function (ids, status) {
                var loadindex = Layer.load();
                $.post({
                    url: 'card_baoming/mutcheck',
                    dataType: 'json',
                    data:{ids:ids,status:status},
                    cache: false,
                    success: function (ret) {
                        if (ret.hasOwnProperty("code")) {
                            var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                            if (ret.code === 1) {
                                //parent.Toastr.success(msg ? msg : '审核成功');
                                Toastr.success(msg ? msg : '审核成功');
                                layer.close(loadindex);
                                //window.location.reload();
                                //Layer.closeAll();
                                $(".btn-refresh").trigger("click");
                                //var index = parent.Layer.getFrameIndex(window.name);
                                //parent.Layer.close(index);
                            } else {
                                Toastr.error(msg ? msg : '审核失败');
                                layer.close(loadindex);
                            }
                        } else {
                            Toastr.error(__('Unknown data format'));
                            layer.close(loadindex);
                        }
                    }, error: function () {
                        Toastr.error(__('Network error'));
                        layer.close(loadindex);
                    }
                });
            };
            $(document).on("click", ".btn-check", function () {
                var ids = Table.api.selectedids(table);
                //var page = table.bootstrapTable('getData');
                //var all = table.bootstrapTable('getOptions').totalRows;
                //console.log(ids, page, all);
                Layer.confirm("请选择操作", {
                    title: '批量审核',
                    btn: ["通过", "拒绝"],
                    success: function (layero, index) {
                        //$(".layui-layer-btn a", layero).addClass("layui-layer-btn0");
                    }
                    , yes: function (index, layero) {
                        layer.close(index);
                        submitForm(ids.join(","), 1);
                        return false;
                    }
                    ,
                    btn2: function (index, layero) {
                        layer.close(index);
                        submitForm(ids.join(","), 2);
                        return false;
                    }
                    ,
                    /*btn3: function (index, layero) {
                        submitForm("all", layero);
                        return false;
                    }*/
                })
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        check: function (){
            Controller.api.bindevent();
            $(".check").click(function(){
                var id = $("#c-id").val();
                var result = $("#c-result").val();
                //var result2 = $("#c-result2").val();
                var status = $(this).attr('data-status');
                /*if (!result) {
                    layer.alert('请输入意见');
                    return false;
                }*/
                var msg = status==1?'确认通过吗？':'确认不通过吗？'
                Layer.confirm(msg, function (index) {
                    layer.close(index);
                    var loadindex = Layer.load();
                    $(".check").addClass("disabled");
                    $.post({
                        url: '',
                        dataType: 'json',
                        data:{id:id,status:status},
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    parent.Toastr.success(msg ? msg : '审核成功');
                                    //Toastr.success(msg ? msg : '审核成功');
                                    //layer.close(loadindex);
                                    //window.location.reload();
                                    //Layer.closeAll();
                                    parent.$(".btn-refresh").trigger("click");
                                    var index = parent.Layer.getFrameIndex(window.name);
                                    parent.Layer.close(index);
                                } else {
                                    Toastr.error(msg ? msg : '审核失败');
                                    layer.close(loadindex);
                                }
                                $(".check").removeClass("disabled");
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                                $(".check").removeClass("disabled");
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                            $(".check").removeClass("disabled");
                        }
                    });
                });
            })
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                intro: function (value, row, index) {
                    var intro = value.substring(0,35);
                    return '<span data-toggle="tooltip" title="'+value+'">'+intro+'</span>';
                },
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="#FF9800">待审核</font>';
                    }else if(value==1){
                        return '<font color="green">审核通过</font>';
                    }else{
                        return '<font color="red">审核拒绝</font>';
                    }
                },
            }
        }
    };
    return Controller;
});
