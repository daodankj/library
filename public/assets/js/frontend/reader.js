define(['jquery', 'bootstrap', 'frontend', 'form', 'template'], function ($, undefined, Frontend, Form, Template) {
    var validatoroptions = {
        invalid: function (form, errors) {
            $.each(errors, function (i, j) {
                Layer.msg(j);
            });
        }
    };
    var Controller = {
        index: function (){

        },
        qiandao_list (){
            $("#qdbtn").click(function(){
                //Layer.confirm('确认签到吗？', function (index) {
                    //layer.close(index);
                    var loadindex = Layer.load();
                    $.post({
                        url: '/index/reader/qiandao',
                        dataType: 'json',
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    Toastr.success(msg ? msg : '签到成功');
                                    layer.close(loadindex);
                                    window.location.reload();
                                } else {
                                    Toastr.error(msg ? msg : '签到失败');
                                    layer.close(loadindex);
                                }
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                        }
                    });
                //});
            })
        },
        baoming: function (){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        baoming_edit: function (){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        choose_book: function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                //Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                //});
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        my_word: function(){
            $("#submit").attr('disabled',false);
            $("#c-content").change(function(){
                var info = $("#c-content").val();
                $("#wnum").html(info.length);
                if(info.length>150){
                    $("#submit").attr('disabled',true);
                    $("#wnum").attr('color','red');
                }else{
                    $("#submit").attr('disabled',false);
                    $("#wnum").attr('color','#000000');
                }
            });
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        word_info:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     //window.location.href=res.url;//成功跳转首页面
                     window.location.reload();
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        goods_info: function(){
            $("#dh").click(function(){
                var id = $(this).attr('data-id');
                Layer.confirm('确认兑换吗？', function (index) {
                    layer.close(index);
                    var loadindex = Layer.load();
                    $.post({
                        url: '/index/reader/good_dh/id/'+id,
                        dataType: 'json',
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    Toastr.success(msg ? msg : '兑换成功');
                                    layer.close(loadindex);
                                    //window.location.reload();
                                    window.location.href='/index/reader/index';//成功跳转首页面
                                } else {
                                    Toastr.error(msg ? msg : '兑换失败');
                                    layer.close(loadindex);
                                }
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                        }
                    });
                });
            })
        },
        order_check:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     //window.location.href=res.url;//成功跳转首页面
                     window.location.reload();
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        card_baoming: function (){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                    window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
            $("#noagree").click(function () {
                window.history.back(-1);//返回
            });
            $("#agree").click(function () {
                $(".remark").hide();
                $(".forminfo").show();
            });
        },
        card_index:function () {
            $(".intro_tk").click(function () {
                layer.open({
                    type: 2,
                    title: '使用说明',
                    shadeClose: true,
                    shade: 0.8,
                    area: ['80%', '520px'],
                    content: 'card_intro' //iframe的url
                });
            });
        }
    };
    return Controller;
});
